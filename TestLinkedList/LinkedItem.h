#pragma once
class LinkedItem
{
	int item;
	LinkedItem *nextItem;
public:
	void setItem(int _item);
	int getItem();
	LinkedItem* getNextItem();
	void setNextItem(LinkedItem* _nextItem);

	LinkedItem();
	~LinkedItem();
};

