#include<iostream>
#include "LinkedList.h"

using namespace std;

void searchItem(LinkedList* list, int startIdx, int endIdx, int item) {
	int idx = (startIdx + endIdx) / 2;
	int elem = list->getItemAtIndex(idx);



	if (item < elem) {
		if (idx == startIdx) {
			list->addNewItem(item, idx);
			return;
		}
		else
			searchItem(list, startIdx, idx, item);
	}
	else if (item > elem) {
		if (idx == endIdx) {
			list->addNewItem(item, idx+1);
			return;
		}
		else
			searchItem(list, idx + 1, endIdx, item);
	}
	else if (item == elem) {
		list->addNewItem(item, idx);
		return;
	}
}

void InsertItem(LinkedList* list, int i) {
	int size = list->getListSize();
	searchItem(list, 0, size - 1, i);
}

int main() {
	LinkedList* list = new LinkedList;
	list->appendNewItem(10);
	list->appendNewItem(20);
	list->appendNewItem(30);
	list->appendNewItem(40);
	list->appendNewItem(50);
	list->appendNewItem(60);
	list->appendNewItem(70);
	list->appendNewItem(80);
	list->appendNewItem(90);

	InsertItem(list, 5);
	InsertItem(list, 105);
	InsertItem(list, 40);
	InsertItem(list, 55);

	list->printLinkedList();

	char c;
	cin >> c;

	return 0;
}