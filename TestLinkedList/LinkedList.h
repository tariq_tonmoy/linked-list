#pragma once
#include<iostream>
#include "LinkedItem.h"


class LinkedList
{
	LinkedItem* StartItem;
	int listSize;
public:
	bool isListEmpty();
	LinkedItem* getStartItem();
	void appendNewItem(int _item);
	void addNewItem(int _item, int _index);
	int getListSize();
	int getItemAtIndex(int _index);
	void printLinkedList();

	LinkedList();
	~LinkedList();
};

