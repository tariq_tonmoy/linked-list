#include "LinkedList.h"

bool LinkedList::isListEmpty() {
	return this->listSize < 1 ? true : false;
}

LinkedItem* LinkedList::getStartItem() {
	return this->StartItem;
}

void LinkedList::appendNewItem(int _item) {
	if (this->isListEmpty()) {
		this->StartItem = new LinkedItem;
		this->StartItem->setItem(_item);
		this->listSize++;
	}
	else {
		this->addNewItem(_item, listSize);

	}
}

void LinkedList::addNewItem(int _item, int _index) {
	if (_index > this->listSize)
		return;

	LinkedItem* next_item = this->StartItem;
	LinkedItem* prev_item = this->StartItem;

	for (int i = 0; i < _index; i++) {
		prev_item = next_item;
		next_item = next_item->getNextItem();
	}
	LinkedItem* item = new LinkedItem;
	item->setItem(_item);
	item->setNextItem(next_item);
	if (_index > 0)
		prev_item->setNextItem(item);
	else
		this->StartItem = item;
	listSize++;
}

int LinkedList::getItemAtIndex(int _index) {
	if (_index >= this->listSize || _index < 0 || this->isListEmpty())
		return 0;
	LinkedItem* item = this->StartItem;
	for (int i = 0; i < _index; i++) {
		item = item->getNextItem();
	}
	return item->getItem();
}

int LinkedList::getListSize() {
	return this->listSize;
}

void LinkedList::printLinkedList() {
	for (int i = 0; i < this->listSize; i++) {
		std::cout << this->getItemAtIndex(i)<<"\n";
	}
}

LinkedList::LinkedList()
{
	this->listSize = 0;
}


LinkedList::~LinkedList()
{
}
